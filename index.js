"use strict";
document.addEventListener("DOMContentLoaded", setup);

const postContainer = document.querySelector(".posts-container");
const loading = document.querySelector(".loader");
const filter = document.querySelector("#filter");
let limit = 6;
let page = 1;
const url = `https://jsonplaceholder.typicode.com/posts?_limit=${limit}&page=${page}`;

async function getPosts() {
  const promise = await fetch(url);
  const data = await promise.json();
  return data;
}
// ...Initial setup code...

function setup() {
  showPosts();
}

async function showPosts() {
  const posts = await getPosts();
  posts.forEach((post) => {
    createObjects(post);
  });
}

// I must remove setTimeout() to better implementation
function showLoading() {
  loading.classList.add("show");

  setTimeout(() => {
    loading.classList.remove("show");
    setTimeout(() => {
      page++;
      showPosts();
    }, 300);
  }, 1000);
}

function createObjects(posts) {
  let post = document.createElement("div");
  let number = document.createElement("div");
  let postinfo = document.createElement("div");
  let postTitle = document.createElement("h2");
  let postBody = document.createElement("p");
  number.className = "number";
  post.className = "post";
  postinfo.className = "post-info";
  postTitle.className = "post-title";
  postBody.className = "post-body";
  postTitle.textContent = posts.title;
  postBody.textContent = posts.body;
  number.textContent = posts.id;
  postinfo.appendChild(postTitle);
  postinfo.appendChild(postBody);
  post.appendChild(number);
  post.appendChild(postinfo);
  postContainer.appendChild(post);
}

window.addEventListener("scroll", () => {
  const { scrollTop, scrollHeight, clientHeight } = document.documentElement;

  if (scrollTop + clientHeight >= scrollHeight - 5) {
    showLoading();
  }
});

filter.addEventListener("input", filterPosts);

function filterPosts(e) {
  let filterValue = e.target.value.toUpperCase();
  let posts = document.querySelectorAll(".post");
  posts.forEach((post) => {
    const title = post.querySelector(".post-title").textContent.toUpperCase();
    const body = post.querySelector(".post-body").textContent.toUpperCase();

    if (title.indexOf(filterValue) > -1 || body.indexOf(filterValue) > -1) {
      console.log(filterValue);
      post.style.display = "flex";
    } else {
      post.style.display = "none";
    }
  });
}
